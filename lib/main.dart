import 'package:flutter/material.dart';
import 'package:ghflutter/strings.dart';
import 'package:ghflutter/ghflutter.dart';

void main() => runApp(new GHFlutterApp());


class GHFlutterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: Strings.appTitle,
      theme: new ThemeData(primaryColor: Colors.indigo),
      home:  GHFlutter(),
    );
  }
}



