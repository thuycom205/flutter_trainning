import 'package:flutter/material.dart';
import 'package:ghflutter/strings.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ghflutter/member.dart';

class GHFlutter extends StatefulWidget {
  @override
  createState() => new GHFlutterState();

}

class GHFlutterState extends State<GHFlutter> {
  var _member = <Member>[];
  final _biggerFont = const TextStyle(fontSize: 18.0);

  _loadData() async {
    String url = "https://api.github.com/orgs/raywenderlich/members";
    http.Response response = await http.get(url);
    setState(() {
      final membersJSON = json.decode(response.body);
      for(var memberJSON in membersJSON){
        final member = new Member(memberJSON["login"], memberJSON["avatar_url"]);
        _member.add(member);
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadData();
  }

  Widget _buildRow(int i) {
    return new Padding(
      padding: const EdgeInsets.all(10.0),
      child: new ListTile(
        title: new Text("${_member[i].login}", style: _biggerFont,),
        leading: new CircleAvatar(
          backgroundColor: Colors.red,
          backgroundImage: new NetworkImage(_member[i].avatarUrl),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(Strings.appTitle),
        ),
        body: new ListView.builder(
            padding: const EdgeInsets.all(15.0),
            itemCount: _member.length *2,
            itemBuilder: (BuildContext context, int position){
              if(position.isOdd){
                return new Divider();
              }

              final index = position ~/2;

              return _buildRow(index);
            }
        )
    );
  }
}